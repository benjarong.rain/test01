const Route = require("express").Router();

const {
    HomeController,
    TestController
} = require("../controllers");

Route.get("/", HomeController.index);
Route.post("/create", TestController.create);
Route.post("/list", TestController.list);
Route.get("/detail/:id", TestController.detail);
Route.patch("/edit/:id", TestController.editItem);
Route.get("/detail_edit/:id", TestController.detail_edit);
Route.delete("/delete/:id", TestController.delete);
module.exports = Route;