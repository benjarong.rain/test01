const { Types } = require("mongoose");
const {
    MyCollection,
} = require("../models");

module.exports.GetById = async ({
    _id = new Types.ObjectId(),
}) => {
    console.log(`_id`, _id);
    try {
        if (!Types.ObjectId.isValid(_id)) {
            throw "_id is not valid"
        }
        const records = await MyCollection.aggregate([
            {
                $match: {
                    _id: new Types.ObjectId(_id)
                }
            }
        ]).exec();
        return [null, records.length > 0 ? records[0] : {}];
    } catch (error) {
        return [`${error.message || error}`, null];
    }
}