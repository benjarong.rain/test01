const fs = require("fs");
const path = require("path");

module.exports = () => {
    if (process.env.NODE_ENV == undefined || process.env.NODE_ENV == "") {
        const PathOfEnv = `${__dirname}/../.`;
        require("dotenv").config({
            path: path.join(PathOfEnv, '.env')
        });
    }
}