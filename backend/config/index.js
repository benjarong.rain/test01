module.exports = {
    name: 'test',
    version: '1.0.0',
    env: process.env.NODE_ENV || 'development',
    serverSettings: {
        port: process.env.APP_PORT || 8080
    },
    dbSettings: {
        db: process.env.DB_NAME,
        server: process.env.DB_SERVER,
    },
    tokenSettings: {
        publicKey: process.env.JWT_TOKEN_SETTING_PUBLIC_KEY,
        apiSecretKey: process.env.YOU_SERVICE_API_KEY,
    }
}