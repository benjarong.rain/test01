const _ = require("lodash");
let response = {
    "statusCode": 0,
    "result": null
}

module.exports.index = (req, res) => {
    try {
        response = {
            "statusCode": 200,
            "result": {
                "message": "Hello World"
            }
        }
    } catch (error) {
        const errorMessage = error.message || error;
        response = {
            "statusCode": 500,
            "result": {
                "message": errorMessage
            }
        }
    }
    res.status(response.statusCode).send(response.result);
}