const HomeController = require("./home.controller");
const TestController = require("./test.controller");

module.exports = {
    HomeController,
    TestController,
}