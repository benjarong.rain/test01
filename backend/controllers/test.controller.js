const _ = require("lodash");
let response = {
    "statusCode": 0,
    "result": null
}
const { TestModels } = require('../models/index')
const {
    MyCollectionModule,
} = require("../modules");

module.exports.create = async (req, res) => {
    try {
        const tt = await TestModels.create(req.body)
        response = {
            "statusCode": 200,
            "result": {
                "message": "Create Data success"
            }
        }
    } catch (error) {
        const errorMessage = error.message || error;
        response = {
            "statusCode": 500,
            "result": {
                "message": errorMessage
            }
        }
    }
    res.status(response.statusCode).send(response.result);
}
module.exports.list = async (req, res) => {
    try {
        const { page, limit, search } = req.body
        const skip = (page - 1) * limit;
        let count = 0;
        let testCount = await TestModels.find({ name: { $regex: search } })
            .count();
        count = Math.ceil(testCount / limit);

        const test = await TestModels.aggregate([
            {
                $match: {
                    name: { $regex: search }
                }
            },
            {
                $project: {
                    name: 1
                }
            },
            { $sort: { "update_at": -1 } },
            { $skip: skip },
            { $limit: limit },
        ])
        response = {
            "statusCode": 200,
            "result": {
                total: count,
                item: test
            }
        }
    } catch (error) {
        console.log(error.stack)
        const errorMessage = error.message || error;
        response = {
            "statusCode": 500,
            "result": {
                "message": errorMessage
            }
        }
    }
    res.status(response.statusCode).send(response.result);
}
module.exports.detail = async (req, res) => {
    try {
        const { id } = req.params
        let count = 0;
        let test = await TestModels.findOne({ _id: id }).lean().exec()
        const item =
        {
            "_id": test._id,
            "name": test.name,
            text: test.item.find(e => e.type == 'text'),
            date: test.item.find(e => e.type == 'date')
        }

        response = {
            "statusCode": 200,
            "result": item
        }
    } catch (error) {
        console.log(error.stack)
        const errorMessage = error.message || error;
        response = {
            "statusCode": 500,
            "result": {
                "message": errorMessage
            }
        }
    }
    res.status(response.statusCode).send(response.result);
}
module.exports.detail_edit = async (req, res) => {
    try {
        const { id } = req.params
        let test = await TestModels.findOne({ _id: id }).lean().exec()
        response = {
            "statusCode": 200,
            "result": test
        }
    } catch (error) {
        console.log(error.stack)
        const errorMessage = error.message || error;
        response = {
            "statusCode": 500,
            "result": {
                "message": errorMessage
            }
        }
    }
    res.status(response.statusCode).send(response.result);
}
module.exports.editItem = async (req, res) => {
    try {
        const { id } = req.params
        const item = req.body
        if (item) {
            const tt = await TestModels.updateOne({ _id: id },
                {
                    $set: {
                        item: item
                    }
                })
            response = {
                "statusCode": 200,
                "result": {
                    "message": "Update Data success"
                }
            }
        }

    } catch (error) {
        const errorMessage = error.message || error;
        response = {
            "statusCode": 500,
            "result": {
                "message": errorMessage
            }
        }
    }
    res.status(response.statusCode).send(response.result);
}
module.exports.delete = async (req, res) => {
    try {
        const { id } = req.params
        if (id) {
            const tt = await TestModels.deleteOne({ _id: id })
            response = {
                "statusCode": 200,
                "result": {
                    "message": "delete Data success"
                }
            }
        }
    } catch (error) {
        const errorMessage = error.message || error;
        response = {
            "statusCode": 500,
            "result": {
                "message": errorMessage
            }
        }
    }
    res.status(response.statusCode).send(response.result);
}