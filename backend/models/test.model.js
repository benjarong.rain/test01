const mongoose = require("mongoose");

const MyCollectionSchema = new mongoose.Schema({
    name: { type: String },
    item: [],
    create_at: { type: Date, required: true, default: Date.now },
    update_at: { type: Date, required: true, default: Date.now }

}, {
    strict: true
});

module.exports = mongoose.model("test", MyCollectionSchema, "test");