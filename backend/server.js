require("./config/env.config")();
const config = require("./config");
const express = require("express");
const app = express();
app.disable("x-powered-by");
const CorsMiddleware = require("./middleware/cors.middleware");
app.use(CorsMiddleware);
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const morgan = require("morgan");
app.use(bodyParser.json());

const APP_PORT = config.serverSettings.port || 3000;
app.use(morgan("combined"));
app.use(`/${config.name}`, require("./routes"));
app.listen(APP_PORT, () => {
    const DB_NAME = config.dbSettings.db;
    const DB_SERVER = config.dbSettings.server;
    const MAX_POOL_SIZE = Number(process.env.MAX_POOL_SIZE || config.dbSettings.poolSize || 5);
    if (DB_NAME && DB_SERVER) {
        const DB_URL = `mongodb://${DB_SERVER}/${DB_NAME}`;
        mongoose.connect(DB_URL);
        mongoose.connection.on('connected', () => {
            console.log(`DATABASE IS SUCCESSFULLY CONNECTED.`);
        });
        mongoose.connection.on('error', (error) => {
            console.log(`CONNECTING DATABASE FAILS: ${error}.`);
        })
        mongoose.connection.on('reconnected', function () {
            console.log('DATABASE IS RECONNECTED.');
        });

        mongoose.connection.on('disconnected', function () {
            console.log('DATABASE IS DISCONNECTED.');
            mongoose.connect(DB_URL, {
                maxPoolSize: MAX_POOL_SIZE
            })
        });
    }
    console.log(`=====SERVER IS SUCCESSFULLY RUNNING AT ${APP_PORT}=====`);
})

module.exports = app
