import axios from 'axios';

const API_URL = 'http://localhost:8080/test'; // Replace with your API base URL

export default {
  create(data) {
    return axios.post(`${API_URL}/create`,data);
  },
  list(data) {
    return axios.post(`${API_URL}/list`,data);
  },
  detail(data) {
    return axios.get(`${API_URL}/detail/${data}`);
  },
  detail_edit(data) {
    return axios.get(`${API_URL}/detail_edit/${data}`);
  },
  edit(id,item) {
    return axios.patch(`${API_URL}/edit/${id}`,item);
  },
  delete(data) {
    return axios.delete(`${API_URL}/delete/${data}`);
  },
  
  

  

  // You can define other API calls here
};